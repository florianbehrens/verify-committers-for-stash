package com.tngtech.stash.plugins.verifycommitters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.bitbucket.commit.*;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.idx.CommitIndex;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.UserService;
import org.apache.commons.lang3.StringUtils;

import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;

public class CommitterVerifier implements PreReceiveRepositoryHook {
	private final CommitService commitService;
	private final UserService userService;
	private final GitCommandBuilderFactory commandFactory;
	private final CommitIndex commitIndex;

	public CommitterVerifier(CommitService commitService,
			UserService userService, GitCommandBuilderFactory gitCommandBuilderFactory, CommitIndex commitIndex) {
		this.commitService = commitService;
		this.userService = userService;
		this.commandFactory = gitCommandBuilderFactory;
		this.commitIndex = commitIndex;
	}

	@Override
	public boolean onReceive(RepositoryHookContext context,	Collection<RefChange> refChanges, HookResponse hookResponse) {
		for (RefChange refChange : refChanges) {
			if (!checkRef(context, refChange, hookResponse, context.getRepository())) {
				return false;
			}
		}
		hookResponse.out().println("Everything looks fine. You may pass.");
		return true;
	}

	private boolean checkRef(RepositoryHookContext context, RefChange refChange, HookResponse hookResponse, Repository repository) {
		for (Commit commit : getChanges(context, refChange)) {
			if (!checkChange(commit, hookResponse, repository)) return false;
		}
		return true;
	}

	private boolean checkChange(Commit commit, HookResponse hookResponse, Repository repository) {
		Persons p=getPersons(commit);
		//already in repo
		if(commitIndex.isIndexed(commit.getId(), repository)){
            hookResponse.out().println("Commit \""+commit.getId()+"\" already in repository.");
			return true;
		}
		return validate(p.authorName,p.authorEmail, hookResponse)
				&& validate(p.committerName,p.committerEmail, hookResponse);
	}

	private boolean validate(String userName, String emailAddress, HookResponse hookResponse) {
		hookResponse.out().println("checking: \""+userName+"\" <"+emailAddress+">");

		ApplicationUser user = userService.findUserByNameOrEmail(emailAddress);
		if (user == null) {
			hookResponse.out().println( "Email <" + emailAddress + "> not known to Stash.");
			return false;
		}
		if (!user.getEmailAddress().equalsIgnoreCase(emailAddress)) {
			hookResponse.out().println( "Incorrect email address: <" + emailAddress + "> instead of <" + user.getEmailAddress() + ">.");
			return false;
		}
		if (!user.getDisplayName().equals(userName)) {
			hookResponse.out().println( "Incorrect user name: <" + userName + "> instead of <" + user.getDisplayName() + ">.");
			return false;
		}
		return true;
	}

	private Iterable<Commit> getChanges(RepositoryHookContext context, RefChange refChange) {
		CommitsBetweenRequest b = new CommitsBetweenRequest.Builder(context.getRepository())
		.exclude(refChange.getFromHash())
		.include(refChange.getToHash())
		.build();
		MyCallback callback=new MyCallback();
		commitService.streamCommitsBetween(b, callback);
		return callback.getValues();
	}

	private Persons getPersons(Commit commit) {
		return commandFactory.builder(commit.getRepository()).revList()
				.format(Persons.GIT_FORMAT).limit(1).rev(commit.getId())
				.build(new CommitterHandler()).call();
	}

	class MyCallback implements CommitCallback {
		List<Commit> changes = new ArrayList<Commit>();

		@Override
		public boolean onCommit(@Nonnull Commit change) throws IOException {
			changes.add(change); // TODO check directly at this place?
			return true;
		}

		public Iterable<Commit> getValues() {
			return changes;
		}

		@Override
		public void onEnd(CommitSummary arg0) throws IOException {
		}

		@Override
		public void onStart(CommitContext arg0) throws IOException {
		}
	}

	class Persons {
		public static final String GIT_FORMAT="%cn%x02%ce%x02%an%x02%ae";

		public String committerName;
		public String committerEmail;
		public String authorName;
		public String authorEmail;

		public Persons(String[] pieces) {
			committerName=pieces[0];
			committerEmail=pieces[1];
			authorName=pieces[2];
			authorEmail=pieces[3];
		}
	}

	class CommitterHandler implements CommandOutputHandler<Persons> {
		private Persons persons;

		@Override
		public void process(InputStream output) throws ProcessException {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					output));
			try {
				String line = reader.readLine();
				if (line == null) {
					return;
				}
				if (!line.startsWith("commit")) {
					throw new IllegalStateException( "[" + line + "]: Unexpected output; expected a 'commit' object");
				}
				line = reader.readLine();
				if (line == null) {
					throw new IllegalStateException("Unexpected end of output; no commit details were present");
				}
				String[] pieces = StringUtils.splitPreserveAllTokens(line, '\u0002');
				if (pieces.length != 4) {
					throw new IllegalStateException("Unexpected number of pieces found.");
				}
				persons=new Persons(pieces);
			} catch (IOException e) {
				throw new IllegalStateException("read error.");
			}
		}

		@Override
		@Nullable
		public Persons getOutput() {
			return persons;
		}

		@Override
		public void complete() throws ProcessException {}

		@Override
		public void setWatchdog(Watchdog watchdog) {}
	}
}
